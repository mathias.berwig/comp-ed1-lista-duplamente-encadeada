import javax.swing.*;

public class App {

    public static void main(String[] args) {
        new App();
    }

    private App() {
        JFrame frame = new JFrame("Lista Duplamente Encadeada");
        MainForm mainForm = new MainForm();
        frame.setContentPane(mainForm.panel1);
        frame.setSize(300, 600);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        mainForm.textPane1.setText(runTest());
    }

    private String runTest() {
        //Intalize list
        DoublyLinkedList<Integer> dlist = new DoublyLinkedList<Integer>();
        DoublyLinkedList<String> stringdlist = new DoublyLinkedList<String>();

        StringBuilder sb = new StringBuilder();

        sb.append("----------------------------------------------------------------------");
        sb.append("\nTesting characters");

        //Test Sorted Inserting with strings
        sb.append("\n\n\nInserting A C E B D F");
        stringdlist.sortedAdd("A");
        stringdlist.sortedAdd("C");
        stringdlist.sortedAdd("E");
        stringdlist.sortedAdd("B");
        stringdlist.sortedAdd("D");
        stringdlist.sortedAdd("F");
        sb.append("\n").append(stringdlist.toString());

        //Testing sorted remove on integer Doubly Linked list
        sb.append("\n\n\nRemoving value D from String List");
        stringdlist.sortedRemove("D");
        sb.append("\n").append(stringdlist.toString());

        sb.append("\n\n\n----------------------------------------------------------------------");
        sb.append("\nTesting integers");

        //Test Sorted Inserting with integers
        sb.append("\n\n\nInserting 1 5 7 9 3 2");
        dlist.sortedAdd(1);
        dlist.sortedAdd(5);
        dlist.sortedAdd(7);
        dlist.sortedAdd(9);
        dlist.sortedAdd(3);
        dlist.sortedAdd(2);
        sb.append("\n").append(dlist.toString());

        // Test sorted remove on integer doubly linked list
        sb.append("\n\n\nRemoving value 3 from List");
        dlist.sortedRemove(3);
        sb.append("\n").append(dlist.toString());

        sb.append("\n\n\nRemoving value 2 from List");
        dlist.sortedRemove(2);
        sb.append("\n").append(dlist.toString());

        sb.append("\n\n\nRemoving value 4 (inexistent) from List");
        dlist.sortedRemove(4);
        sb.append("\n").append(dlist.toString());

        return sb.toString();
    }
}
