# Lista Duplamente Encadeada

Trabalho da disciplina Estrutura de Dados 1 na UNIJUÍ.

Este projeto é um fork de https://github.com/JeremySawh/Doubly-Linked-List.

## Readme original

Doubly-Linked-List
Circular Doubly Linked List Data Struture with dummy node implemented in Java.

![doubly link list](https://cloud.githubusercontent.com/assets/10781590/9506288/336cd434-4c15-11e5-9a03-2bdc8f84fe6f.jpg)

Example of Sorted Insert and removal algortihm's for the circular Doubly-Linked list:

![image](https://cloud.githubusercontent.com/assets/10781590/9640867/9bfa3868-5181-11e5-8427-227ce9c60cfb.png)
