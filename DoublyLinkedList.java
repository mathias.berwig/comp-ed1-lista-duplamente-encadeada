/**
 * Lista Circular Duplamente Encadeada com nó burro.
 *
 * @author JeremySawh
 * @version 2015-07-08 3:25
 */
public class DoublyLinkedList<T extends Comparable<T>> {

    private int numElements;
    private Node<T> head;
    private Node<T> dummy;


    /**
     * Construtor da classe. Inicializa todos os valores e cria o nó burro.
     */
    public DoublyLinkedList() {
        dummy = new Node<T>();
        dummy.setNext(dummy);
        dummy.setPrevious(dummy);
        dummy.setitem(null);
        head = dummy.getNext();
    }


    /**
     * @return retorna um número inteiro com o tamanho da lista.
     */
    public int size() {
        return numElements;
    }


    /**
     * Obtém um elemento no índice especificado.
     *
     * @param index índice do elemento.
     * @return elemento no índice especificado.
     */
    public T get(int index) {
        Node<T> temp = head;
        for (int i = 0; i <= index; i++) {
            temp = temp.getNext();
        }
        return temp.getitem();
    }


    /**
     * Insere um item na sua posição correta da lista ordenada.
     *
     * @param item item a ser adicionado.
     */
    public void sortedAdd(T item) {
        Node<T> cur = dummy.getNext();
        while (cur != dummy && item.compareTo(cur.getitem()) > 0) {//
            cur = cur.getNext();
        }

        Node<T> temp = new Node<T>();
        temp.setNext(cur);
        temp.setPrevious(cur.getPrevious());
        cur.getPrevious().setNext(temp);
        cur.setPrevious(temp);
        temp.setitem(item);
        numElements++;
    }


    /**
     * Remove um item da lista ordenada, caso este for encontrado.
     *
     * @param item item a ser removido.
     */
    public void sortedRemove(T item) {

        for (Node<T> cur = dummy.getNext(); cur != dummy; cur = cur.getNext()) {
            if (item.equals(cur.getitem())) {
                cur.getPrevious().setNext(cur.getNext());
                cur.getNext().setPrevious(cur.getPrevious());
                numElements--;
                return;
            }
        }
        // Item not found
    }


    /**
     * Traversa a lista e retorna seus valores em uma string, separados por espaço.
     *
     * @return representação textual da lista.
     */
    public String toString() {
        Node<T> temp = dummy.getNext();
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < numElements; i++) {
            s.append(temp.getitem()).append(" ");
            temp = temp.getNext();
        }
        return s.toString();
    }
}
